#!/bin/bash

source ../settings.sh

echo "Building hello world timeline."
echo

docker run \
	--rm \
	--user "$(id -u):$(id -g)" \
	--name $CONTAINERNAME \
	--volume "${PWD}:/mw" \
	$IMAGENAME \
		"testdoc/hello-world.mw" \
		--destination "testdoc/hello-world.timeline.html"
