# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog] and this project adheres to [Semantic Versioning].


## [1.0.0-1.2.4] - 2024-02-23

- initial version

### Added

- markwhen `1.2.4`


[1.0.0-1.2.4]: https://gitlab.com/etg-docker/markwhen/-/tags/1.0.0-1.2.4

[Keep a Changelog]: https://keepachangelog.com/en/1.1.0/
[Semantic Versioning]: https://semver.org/spec/v2.0.0.html
