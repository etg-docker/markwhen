#!/bin/bash

source ../settings.sh

echo "Build image $IMAGENAME"
echo

docker build \
	--progress=plain \
	--tag $IMAGENAME:$DOCKER_TAG_CURRENT \
	.

docker tag $IMAGENAME:$DOCKER_TAG_CURRENT $IMAGENAME:$DOCKER_TAG_LATEST
docker tag $IMAGENAME:$DOCKER_TAG_CURRENT $IMAGENAME:$DOCKER_TAG_INTERNAL
